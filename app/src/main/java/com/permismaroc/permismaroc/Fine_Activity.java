package com.permismaroc.permismaroc;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Ayoub on 4/18/2017.
 */

public class Fine_Activity extends AppCompatActivity{
    /*** don't modify the order it's in relaton with array string of xml **/
    private int[] array_of_fine_title = {R.string.fine_of_300dh_title,R.string.fine_of_500dh_title,
                                                     R.string.fine_of_700dh_title,R.string.misdemeanors_title};
    ViewGroup fine_viewgrp;
    TextView fine_title_textview;
    Intent toFineSpecAct;

    public static Fine_Activity newInstance(){
        return new Fine_Activity();
    }
    public int[][][] array_pos = {array_of_spec_fine_of_300dh,array_of_spec_fine_of_500dh,array_of_spec_fine_of_700dh,array_of_spec_fine_of_misdemeanors};

    public final static String KEY_FINE = "fine";

    private final static int[][] array_of_spec_fine_of_300dh = {{4,R.array.four_point_of_fine_of_300dh},{3,R.array.three_point_of_fine_of_300dh},
                                                                {2,R.array.two_point_of_fine_of_300dh},{0,R.array.zero_point_of_fine_of_300dh}};
    private final static int[][] array_of_spec_fine_of_500dh = {{3,R.array.three_point_of_fine_of_500dh},{2,R.array.two_point_of_fine_of_500dh},
                                                                {1,R.array.one_point_of_fine_of_500dh},{0,R.array.zero_point_of_fine_of_500dh}};
    private final static int[][] array_of_spec_fine_of_700dh = {{4,R.array.four_points_of_fine_of_700dh},{3,R.array.three_point_of_fine_of_700dh},
                                                                {0,R.array.zero_point_of_fine_of_700dh}};
    private final static int[][] array_of_spec_fine_of_misdemeanors = {{0,R.array.misdemeanors_contents}};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fine_layout);
        fine_viewgrp = (ViewGroup) findViewById(R.id.fine_layout);

        /** Populate the scrollview **/
        for(int i = 0 ; i<array_of_fine_title.length ; i++){
            final View fine_item_view = getLayoutInflater().inflate(R.layout.fine_item_layout,null);
            fine_title_textview = (TextView) fine_item_view.findViewById(R.id.fine_title_textview);
            /** set the id of driving_item_view **/
            fine_item_view.setId(i);
            /** get title of fine **/
            String fine_item_title = getResources().getString(array_of_fine_title[i]);
            /** put dada in each component **/
            fine_title_textview.setText(fine_item_title);
            /** add driving_item_view to scrollview **/
            fine_viewgrp.addView(fine_item_view);
            fine_item_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toFineSpecAct = new Intent(Fine_Activity.this,FineItem_Activity.class);
                    toFineSpecAct.putExtra(KEY_FINE,fine_item_view.getId());
                    startActivity(toFineSpecAct);
                }
            });
        }
    }
}
