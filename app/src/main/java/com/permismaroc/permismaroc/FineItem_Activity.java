package com.permismaroc.permismaroc;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Ayoub on 4/16/2017.
 */

public class FineItem_Activity extends AppCompatActivity implements View.OnClickListener{
    TextView spec_fine_title;
    TextView spec_fine_point;
    TextView spec_fine_desc;
    View next_fine,prev_fine;
    /** get the fine description gor a specific fine **/
    String[] fine_desc;
    /** get the fine point for a specific fine **/
    int fine_point;
    /** counter of view prev & next **/
    int counter=0;
    /** size of array that contains ref to string array **/
    int all_fine_details_size;
    /** all fine details **/
    int[][] fine_Details;
    /** counter of each fine get it from string array **/
    int fine_desc_counter=-1;
    /** fine title **/
    String fine_title;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fine_spec_item_layout);
        spec_fine_title = (TextView)findViewById(R.id.fine_spec_title);
        spec_fine_point = (TextView)findViewById(R.id.spec_fine_point);
        spec_fine_desc = (TextView)findViewById(R.id.spec_fine_desc);
        next_fine = findViewById(R.id.next_fine_view);
        prev_fine = findViewById(R.id.prev_fine_view);

        /** get intent **/
        Intent getRes = getIntent();
        int arrayPos = getRes.getIntExtra(Fine_Activity.KEY_FINE,-1);
        /** get details of specific fine **/
        if(arrayPos!=-1){
            fine_title = getResources().getStringArray(R.array.fine_condition_by_x_dh)[arrayPos];
            spec_fine_title.setText(fine_title);
            fine_Details = Fine_Activity.newInstance().array_pos[arrayPos];
            all_fine_details_size = fine_Details.length;
            next_fine.setOnClickListener(this);
            prev_fine.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.next_fine_view  :
                if(counter<all_fine_details_size){
                    if(fine_desc_counter == getResources().getStringArray(fine_Details[counter][1]).length-1 && counter==all_fine_details_size-1){
                        return;
                    }
                    fine_desc_counter++;
                    if(fine_desc_counter<getResources().getStringArray(fine_Details[counter][1]).length){
                        setComponent();
                    }
                    else{
                        fine_desc_counter=0;
                        counter++;
                        if(counter==all_fine_details_size)
                            return;

                        setComponent();
                    }
                }
                break;
            case R.id.prev_fine_view :
                if(counter>=0){
                    if(fine_desc_counter<=0 && counter==0){
                        return;
                    }
                    fine_desc_counter--;
                    if(fine_desc_counter>=0){
                        setComponent();
                    }
                    else{
                        counter--;
                        if(counter<0)
                            return;

                        fine_desc_counter = getResources().getStringArray(fine_Details[counter][1]).length-1;
                        setComponent();
                    }
                }
                break;
        }
    }

    private void setComponent(){
        fine_point = fine_Details[counter][0];
        spec_fine_point.setText(String.valueOf(fine_point));
        fine_desc = getResources().getStringArray(fine_Details[counter][1]);
        spec_fine_desc.setText(fine_desc[fine_desc_counter]);
    }
}
