package com.permismaroc.permismaroc;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

/**
 * Created by Ayoub on 4/12/2017.
 */

public class Signs_activity extends AppCompatActivity  implements AdapterView.OnItemClickListener {
    GridView signs_gridview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signs_layout);
        /** get the position of item_clicked from intent **/
        Intent intent = getIntent();
        int position_signs = intent.getIntExtra(LearnDriving_Activity.KEY_SIGNS,-1);
        
        signs_gridview = (GridView) findViewById(R.id.signs_gridview);
            
        MySignsAdapter adapter = new MySignsAdapter(this,getResources().getStringArray(LearnDriving_Activity.arrays_of_signs_img[position_signs]),getResources().getStringArray(LearnDriving_Activity.array_of_signs_title[position_signs]));
        signs_gridview.setAdapter(adapter);
        signs_gridview.setOnItemClickListener(this);
    }

    // on response to tapping, display a high-resolution version of the chosen thumbnail
    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

    }



}
