package com.permismaroc.permismaroc;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
/**
 * Created by Ayoub on 4/12/2017.
 */

public class MySignsAdapter extends BaseAdapter{
    private Context context;
    private String[] signs_smallImages;
    private String[] signs_title;

    public MySignsAdapter(Context context, String[] signs_smallImages, String[] signs_title){
        this.context = context;
        this.signs_smallImages = signs_smallImages;
        this.signs_title = signs_title;
    }

    @Override
    public int getCount() {
        return signs_title.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View signs_grid;
        LayoutInflater inflater = (LayoutInflater)context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            //signs_grid = new View(context);
            signs_grid = inflater.inflate(R.layout.signs_item_layout, null);
            TextView textView = (TextView) signs_grid.findViewById(R.id.signs_title);
            ImageView imageView = (ImageView)signs_grid.findViewById(R.id.signs_img);
            textView.setText(signs_title[position]);
            imageView.setImageResource(context.getResources().getIdentifier(signs_smallImages[position],"drawable",context.getPackageName()));
        } else {
            signs_grid = convertView;
        }

        return signs_grid;
    }
}
