package com.permismaroc.permismaroc;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Created by Ayoub on 4/11/2017.
 */

public class LearnDriving_Activity extends AppCompatActivity{

    ViewGroup driving_viewgrp;
    TextView signs_title_textview,signs_count_textview;
    Intent toSignsAct;
    public final static String KEY_SIGNS= "signs";
    /*** don't modify the order it's in relaton with array string of xml **/
    public final static int[] array_of_signs_title = {R.array.signs_against_prevention,R.array.prevention_signs,R.array.end_signs_of_coercion,
                               R.array.signs_of_coercion,R.array.extension_marks,R.array.signs_of_attention,R.array.simulation,
                               R.array.traffic_police_signals_and_royal_gendarmerie};
    public final static int[] arrays_of_signs_img = {R.array.signs_against_prevention_img,R.array.prevention_signs_img,R.array.end_signs_of_coercion_img,
                               R.array.signs_of_coercion_img,R.array.extension_marks_img,R.array.signs_of_attention_img,R.array.simulation_img,
                               R.array.traffic_police_signals_and_royal_gendarmerie_img};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.learn_driving_layout);
        driving_viewgrp = (ViewGroup) findViewById(R.id.driving_layout);
        /** Populate the scrollview **/
        int driving_guide_array_size = getResources().getStringArray(R.array.driving_guide_name).length;
        for(int i = 0 ; i<driving_guide_array_size ; i++){
            final View driving_item_view = getLayoutInflater().inflate(R.layout.learn_driving_item_layout,null);
            signs_title_textview = (TextView) driving_item_view.findViewById(R.id.signs_title_textview);
            signs_count_textview = (TextView) driving_item_view.findViewById(R.id.signs_count_textview);
            /** set the id of driving_item_view **/
            driving_item_view.setId(i);
            /** get title & count of driving_guide **/
            String driving_item_title = getResources().getStringArray(R.array.driving_guide_name)[i];
            int driving_item_count = getResources().getStringArray(array_of_signs_title[i]).length;
            /** put dada in each component **/
            signs_title_textview.setText(driving_item_title);
            signs_count_textview.setText(String.valueOf(driving_item_count));
            /** add driving_item_view to scrollview **/
            driving_viewgrp.addView(driving_item_view);
            driving_item_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(getBaseContext(),"position "+driving_item_view.getId(),Toast.LENGTH_LONG).show();
                    toSignsAct = new Intent(LearnDriving_Activity.this,Signs_activity.class);
                    toSignsAct.putExtra(KEY_SIGNS,driving_item_view.getId());
                    startActivity(toSignsAct);
                }
            });
        }


    }
}
