package com.permismaroc.permismaroc;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

public class Main2Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    FrameLayout frame_feature1,frame_feature2,frame_feature3,frame_feature4;
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        frame_feature1 = (FrameLayout)findViewById(R.id.first_feature_frame);
        frame_feature2 = (FrameLayout)findViewById(R.id.second_feature_frame);
        frame_feature3 = (FrameLayout)findViewById(R.id.third_feature_frame);
        frame_feature4 = (FrameLayout)findViewById(R.id.fourth_feature_frame);

        frame_feature1.setOnClickListener(this);
        frame_feature2.setOnClickListener(this);
        frame_feature3.setOnClickListener(this);
        frame_feature4.setOnClickListener(this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.first_feature_frame :
                break;
            case R.id.second_feature_frame :
                intent = new Intent(Main2Activity.this,LearnDriving_Activity.class);
                startActivity(intent);
                break;
            case R.id.third_feature_frame :
                intent = new Intent(Main2Activity.this,Fine_Activity.class);
                startActivity(intent);
                break;
            case R.id.fourth_feature_frame :
                break;
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_share) {
            // Handle the camera action
        } else if (id == R.id.nav_infos) {

        } else if (id == R.id.nav_rate) {

        } else if (id == R.id.nav_exit) {

        } else if (id == R.id.nav_default) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
